package at.denovo.weatherapp.beans;

import org.simpleframework.xml.Attribute;

public class Pressure {
    @Attribute
    String value;
    @Attribute
    String unit;
    public Pressure() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
