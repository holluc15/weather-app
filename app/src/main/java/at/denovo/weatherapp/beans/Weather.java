package at.denovo.weatherapp.beans;

import org.simpleframework.xml.Attribute;

public class Weather {

    @Attribute(required = false)
    String value;
    @Attribute(required = false)
    String number;
    @Attribute(required = false)
    String icon;
    public Weather() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
