package at.denovo.weatherapp;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import at.denovo.weatherapp.beans.City;
import at.denovo.weatherapp.beans.Humidity;
import at.denovo.weatherapp.beans.Pressure;
import at.denovo.weatherapp.beans.Temperature;
import at.denovo.weatherapp.beans.Weather;
import at.denovo.weatherapp.beans.Wind;

@Root(name="current",strict = false)
public class CurrentCity {

    @Element(required = false)
    public City city;
    @Element(required = false)
    public Temperature temperature;
    @Element(required = false)
    public Pressure pressure;
    @Element(required = false)
    public Humidity humidity;
    @Element(required = false)
    public Weather weather;
    @Element(required = false)
    public Wind wind;
    public CurrentCity() {
    }

}
