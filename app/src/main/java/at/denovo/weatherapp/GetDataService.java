package at.denovo.weatherapp;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetDataService {

    @GET("data/2.5/weather")
    Call<CurrentCity> getCityData(@Query("q") String city, @Query("mode") String mode,@Query("appid") String appid, @Query("units") String units);
}
