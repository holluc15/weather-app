# WeatherApp

This is a simple WeatherApp, which will show weather data, from an entered city. It has been kept simple, to allow an easy navigation for the user.

## Main Screen

An city name has to be entered at the home screen.

After clicking on the 'ok'- button, the app will automatically search for the city's weather data.

<img src="https://gitlab.com/holluc15/weather-app/uploads/83e345b3243fd28d032966d987562c73/home.PNG" width=15%>

## Weather Screen

The application, will then display following data
*  temperature
*  pressure
*  humidity
*  wind strength
*  country
*  weather

<img src="https://gitlab.com/holluc15/weather-app/uploads/1cb1cab08fc2956d1c0bcc71e5ed29ed/Screenshot_20200215-203830.jpg" width=15%>






## Built With

* [Java](https://www.java.com/de/) - The programming language
* [Gradle](https://gradle.org/) - Dependency Management
* [GitLab](https://nodejs.org/en/) - Versioning software 
* [API](https://openweathermap.org/) - Application Programming Interface


## Authors

* **Lukas Holzmann** - *Android Application*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
