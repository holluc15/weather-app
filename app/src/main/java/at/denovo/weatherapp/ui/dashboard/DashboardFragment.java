package at.denovo.weatherapp.ui.dashboard;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import at.denovo.weatherapp.CurrentCity;
import at.denovo.weatherapp.GetDataService;
import at.denovo.weatherapp.R;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class DashboardFragment extends Fragment implements Callback<CurrentCity> {

    private String city;
    private TextView dashboard_city;
    private TextView text_temp;
    private TextView text_pressure;
    private TextView text_humidity;
    private TextView text_weather;
    private TextView text_wind;
    private static final String BASE_URL = "https://api.openweathermap.org/";

    public DashboardFragment(String city) {
        this.city = city;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        dashboard_city = root.findViewById(R.id.text_dashboard);
        text_temp = root.findViewById(R.id.text_temp);
        text_pressure = root.findViewById(R.id.text_pressure);
        text_humidity = root.findViewById(R.id.text_humidity);
        text_weather = root.findViewById(R.id.text_weather);
        text_wind = root.findViewById(R.id.text_wind);
        start();
        return root;
    }




    private void start() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        GetDataService gerritAPI = retrofit.create(GetDataService.class);

        Call<CurrentCity> call = gerritAPI.getCityData(city,"xml","bbd97a6ee3c1ec4072c1539892dfc6fe","metric");
        call.enqueue(this);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResponse(Call<CurrentCity> call, Response<CurrentCity> response) {
        if(response.isSuccessful()) {
            CurrentCity body = response.body();
            dashboard_city.setText(body.city.getName()+" - "+body.city.getCountry());
            text_temp.setText(body.temperature.getValue()+" "+body.temperature.getUnit());
            text_pressure.setText(body.pressure.getValue()+" "+body.pressure.getUnit());
            text_humidity.setText(body.humidity.getValue()+" "+body.humidity.getUnit());
            text_weather.setText(body.weather.getValue());
            text_wind.setText(body.wind.getSpeed().getValue()+" "+body.wind.getSpeed().getUnit());
        } else {
            dashboard_city.setText("No city found!");
        }
    }

    @Override
    public void onFailure(Call<CurrentCity> call, Throwable t) {
        t.printStackTrace();
    }


}