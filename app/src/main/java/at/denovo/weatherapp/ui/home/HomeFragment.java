package at.denovo.weatherapp.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import at.denovo.weatherapp.R;
import at.denovo.weatherapp.ui.dashboard.DashboardFragment;

public class HomeFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        final View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView submit = root.findViewById(R.id.submit_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edittext = root.findViewById(R.id.city_input);
                String city = edittext.getText().toString();
                if(!city.isEmpty()) {
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.slide_in_left,android.R.anim.slide_out_right).add(R.id.container,new DashboardFragment(city)).addToBackStack("DashBoard").commit();
                }else{
                    Toast.makeText(getContext(),"Please enter a city!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        return root;
    }
}