package at.denovo.weatherapp.beans;

import org.simpleframework.xml.Attribute;

public class Speed {
    @Attribute
    String value;
    @Attribute
    String unit;
    @Attribute
    String name;
    public Speed() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
