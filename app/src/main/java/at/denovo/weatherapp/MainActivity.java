package at.denovo.weatherapp;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import java.util.List;
import at.denovo.weatherapp.ui.dashboard.DashboardFragment;
import at.denovo.weatherapp.ui.home.HomeFragment;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getSupportFragmentManager().beginTransaction().add(R.id.container,new HomeFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if(fragments.size() > 0) {
        Fragment last = fragments.get(fragments.size()-1);
        if(last instanceof DashboardFragment) {
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .remove(last)
                    .commit();
        }else{
            super.onBackPressed();
        }
        }else{
            super.onBackPressed();
        }

    }
}
