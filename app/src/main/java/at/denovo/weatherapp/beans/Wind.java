package at.denovo.weatherapp.beans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public class Wind {
    @Element
    Speed speed;
    @Element(required = false)
    String gusts;
    @Element(required = false)
    String direction;

    public Wind() {
    }

    public Speed getSpeed() {
        return speed;
    }

    public void setSpeed(Speed speed) {
        this.speed = speed;
    }
}
